import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="tcdtool-slenne", # Replace with your own username
    version="1.5.1",
    author="Simon Lenne",
    author_email="lennesimon@gmail.com",
    description="Python script for data processing",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/slenne/tcdtool",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.4',
)
