import numpy as np
from scipy.ndimage.filters import gaussian_filter1d

# format du type "+" : (2, fplus), on y met aussi les variable (0, mavar) la fonction doit resortir un scalaire ou numpy array:
operator = {
        '+'    : (2, lambda a, b : a+b) , 
        '-'    : (2, lambda a, b : a-b) , 
        '*'    : (2, lambda a,b : a*b) , 
        '/'    : (2, lambda a,b : a/b) , 
        '**'   : (2, lambda x, p : x**p) , 
        'exp'  : (1, lambda x : np.exp(x)) ,
        'log'  : (1, lambda x : np.log(x)) ,
        'cos'  : (1, lambda x : np.cos(x)) ,
        'sin'  : (1, lambda x : np.sin(x)) ,
        'sqrt' : (1, lambda x : np.sqrt(x)),
        'sign' : (1, lambda x : np.sign(x)),
        'mean' : (1, lambda x : np.mean(x)),
        'rad2deg' : (1, np.rad2deg),
        'deg2rad' : (1, np.deg2rad),
        'min' : (1, np.nanmin),
        'max' : (1, np.nanmax),
        'abs' : (1, lambda x : np.abs(x)),
        'gfilter': (2, lambda a, s : gaussian_filter1d(a, s)),
        'dup' : (1, lambda x : [x, x]),
        'dump' : (1, lambda x : []),
        '[]' : (2, lambda x, y : x[int(y)]),
        } 

operator_tex = {
        '+'    : (2, lambda a, b : '{} + {}'.format(a,b)) , 
        '-'    : (2, lambda a, b : '{} - {}'.format(a,b)) , 
        '*'    : (2, lambda a,b : '{}{}'.format(a,b)) , 
        '/'    : (2, lambda a,b : '\\frac{{{}}}{{{}}}'.format(a,b)) , 
        '**'   : (2, lambda x, p : '{}^{{{}}}'.format(x,p)) , 
        'exp'  : (1, lambda x : '\\exp{{{}}}'.format(x)) ,
        'log'  : (1, lambda x : '\\log{{{}}}'.format(x)) ,
        'cos'  : (1, lambda x : '\\cos{{{}}}'.format(x)) ,
        'sin'  : (1, lambda x :'\\sin{{{}}}'.format(x)) ,
        'sqrt' : (1, lambda x : '\\sqrt{{{}}}'.format(x)),
        'sign' : (1, lambda x : 'sgn{{{}}}'.format(x)),
        'mean' : (1, lambda x : 'mean{{{}}}'.format(x)),
        'rad2deg' : (1, lambda x : 'rad2deg{{{}}}'.format(x)),
        'deg2rad' : (1, lambda x : 'deg2rad{{{}}}'.format(x)),
        'min' : (1, lambda x : 'min{{{}}}'.format(x)),
        'max' : (1, lambda x : 'max{{{}}}'.format(x)),
        'abs' : (1, lambda x : '|{}|'.format(x)),
        'gfilter': (2, lambda x,s : x),
        'dup' : (1, lambda x : [x, x]),
        'dump' : (1, lambda x : []),
        '[]' : (2, lambda x, y : '{}[{}]'.format(x, y)),
        }

def formatexpr(expr):
    if isinstance(expr, str):
        u = expr.split(' ')
        u = list(filter(bool, u)) # list à parser
    elif isinstance(expr, list):
        u = []
        for i,v in enumerate(expr):
            if isinstance(v, str):
                v = v.split(' ')
                v = list(filter(bool, v)) # list à parser
                u.extend(v)
            else :
                u.append(v)
    return u

def label_parser(vx):
    u = []
    while len(vx):
        x = vx.pop(0)
        if x in operator_tex:
            k, f = operator_tex[x]
            param = [ u.pop(-1) for _ in range(k)]
            param.reverse()
            tu = f(*param)
            if isinstance(tu, list):
                u.extend(tu)
            else :
                u.append(tu)
        else :
            u.append(x)
    if len(u) == 1 : #on retourne la liste ou alors le résultat si il existe
        u = u[0]
        u = r'$' + u + r'$'
    return u

