import os,json
import copy
import tcdtool.rpn as rpn
import matplotlib.pyplot as plt
import matplotlib as mpl
from tcdtool.fit_fct import Gfilter, IncrSelect
import numpy as np
mpl.rcParams['text.latex.preamble'] = r'\usepackage{amsmath}' #for \text command
plt.rc('text', usetex=False)
from .read_file import HeaderParser, get_key, label2key
import tcdtool.meta_obj as meta_utils
from glob import glob




class Dat:
    """ Abstract class """
    def __init__(self, title='', **kargs):
        self.title = title
        self._val = []
        self.label = []
        self.key = {}
        self.meta = {} # meta donné calculé dans un script peuvent être enregisté ici
        self._accept_val = None

    def setData(self, val, label, key=None):
        self._val = []
        self.label = []
        self.key = {}
        if key == None or key == {}:
            key = get_key(label)
        n = len(val)
        if not ( len(label) == n and len(key) == n):
            raise ValueError(f"len of val ({n}), label ({len(label)}) and key ({len(key)}) doesn't match")
        for d,l,k in zip(val, label, key):
            tp = np.issubdtype(d.dtype, np.number)
            if not tp or (tp and any(np.isfinite(d))) : # add only colum that have actually data
                self.add(d, l, k)
    def add(self, data, label , key=None):
        """si on ajoute sur la même clé alors les donné sont écrasé, celle-ci est déterminé depuis le label si non précisé """
        #allow add expression
        if len(self._val) > 0  and len(data) != len(self._val[0]): # if there is data they should be of the same size
            raise ValueError("il manque des données. Penser à utiliser init_val=True")
        data = np.asarray(data)
        if key is None:
            key = label2key(label)
        i = self.key.get(key)
        if i is None:
            i = len(self._val)
            self.key[key] = i
            self._val.append(data)
            self.label.append(label)
        else :
            print('overwrite {} → {}'.format(self.label[i], label))
            self._val[i] = data
            self.label[i] = label

    @property
    def accept_val(self):
        if self._accept_val is None:
            return ~np.zeros(self._val[0].shape, bool)
        else :
            return self._accept_val

    @accept_val.setter
    def accept_val(self, val):
        """ check the length of the accept_val before setting it """
        if not val is None and len(val) != len(self._val[0]):
            raise ValueError("The length doesn't match")
        self._accept_val = val

    def hasAcceptVal(self):
        if self._accept_val is None:
            return False
        else :
            return True

    def pop(self, key):
        i = self.key.get(key)
        if i is None:
            print('pas de donné pour {}'.format(key))
        else :
            self._val.pop(i)
            self.label.pop(i)
            self.key.pop(key)
            for k, v in self.key.items():
                if v > i :
                    self.key[k] = v-1


    def _readVal(self, i, init_val=False):
        if init_val or self.accept_val is None:
            x = self._val[i]
        else :
            x = self._val[i][self.accept_val]
        return x

    def getVal(self, vx, label=False, init_val=False): 
        if isinstance(vx, (str,list)):
            vx = rpn.formatexpr(vx)
            u = []
            for i,x in enumerate(vx):
                if isinstance(x, np.ndarray):
                    u.append(x)
                    vx[i] = 'inc'
                elif x in self.key:
                    u.append(self._readVal(self.key[x], init_val))
                    vx[i] = self.label[self.key[x]]
                elif isinstance(x, str) and x.startswith('meta.'):
                    try :
                        xi = { k.replace(' ', '_'):v for k, v in self.meta.items()}
                        for key in x.split('.')[1:]:
                            xi = xi[key]
                            if isinstance(xi, dict):
                                xi = { k.replace(' ', '_'):v for k, v in xi.items()}
                        u.append(xi)
                    except ValueError:
                        raise ValueError("{} inconnue".format(x))
                    vx[i] = x[5:]
                elif x in rpn.operator or ( isinstance(x, tuple) and len(x) == 2 and isinstance(x[1], int) and callable(x[0])) :
                    if x in rpn.operator:
                        v, f = rpn.operator[x]
                    else :
                        f, v = x
                    param = [ u.pop(-1) for _ in range(v)]
                    param.reverse()
                    tu = f(*param)
                    if isinstance(tu, (list, tuple)):
                        u.extend(tu)
                    else :
                        u.append(tu)
                else :
                    try :
                        f = float(x)
                        u.append(f)
                    except ValueError:
                        raise ValueError("{} inconnue".format(x))
            xlab = rpn.label_parser(vx)
            if len(u) == 1 : #on retourne la liste ou alors le résultat si il existe
                u = u[0]
        else :
            u, xlab = self._readVal(vx).copy(), self.label[vx]
        if label:
            return u, xlab
        else:
            return u

    def setKey(self, key, i=None):
        """ set the key 
        - if the column number i is given set the key to the column i
        - if key is a list it will be attribute to the different column starting from zero, The len of the list should
        be the same than the number of value
        """
        if isinstance(i, int):
            self.key[key] = i
        elif isinstance(key, list) and len(self._val) == len(key):
            for i, k in enumerate(key):
                self.key[k] = i

    def setLabel(self, label, i=None):
        """ set the label 
        - if the column number i is given set the label to the column i
        - if label is a list it will be attribute to the different column starting from zero, The len of the list should
        be the same than the number of value
        """
        if isinstance(i, int):
            self.label[i] = label
        elif isinstance(label, list) and len(self._val) == len(label):
            for i, v in enumerate(label):
                self.label[i] = v

    def filter(self, vx, vy, fullscale=False):
        subplots_shape = { 1 : (1,1), 2 : (1,2), 3 : (2,2), 4 : (2,2), 5 : (2,3), 6 : (2,3), 7 : (3,3), 8 : (3,3), 9 : (3,3), 10 : (3,4), 11 : (3,4), 12 : (3,4), 13 : (4,4), 14 : (4,4), 15 : (4,4), 16 : (4,4), }
        #if isinstance(vx, (list, tuple)):
        # if vy is None:
        # else
        y, ylab = self.getVal(vy, label=True, init_val=True)
        if isinstance(y, list):
            nb_tot = len(y)
        else:
            nb_tot = 1
            y = [y]
            ylab = [ylab]
        if vx :
            x, xlab = self.getVal(vx, label=True, init_val=True)
            if nb_tot == 1 :
                x = [x]
                xlab = [xlab]
        else :
            xlab = 'nb of points'
            x = np.arange(nb_tot, y.size)
        selector = []
        def updater():
            for ki in selector:
                ki.redraw()
        fig, ax = plt.subplots(*subplots_shape[nb_tot])
        ax = np.asarray(ax).ravel()
        pfinite = np.logical_and.reduce((*map(np.isfinite, x), *map(np.isfinite, y)))
        p = self.accept_val[pfinite].reshape(1, -1)
        for axi, xlabi, ylabi, xi, yi in zip(ax, xlab, ylab, x, y):
            axi.set_xlabel(xlabi)
            axi.set_ylabel(ylabi)
            u, = axi.plot(xi, yi)
            k = IncrSelect(axi, u, preselect=p, callback=updater, fullscale=fullscale)
            u_err = k.err[0]
            u_err.set_color('r')
            u_err.set_marker('^')
            u_err.set_alpha(1)
            selector.append(k)
        updater()
        plt.show()
        p_update= self.accept_val
        p_update[pfinite] = p.squeeze()
        self.accept_val = p_update
        print('fin du filtrage')

    def plot(self, ax, vx, vy=None, *args, **kargs):
        x, xlab = self.getVal(vx, label=True)
        if not 'label' in kargs :
            kargs['label'] = self.title
        if vy is None:
                f, = ax.plot(x, *args, **kargs)
                ax.set_xlabel('nb point')
                ax.set_ylabel(xlab)
        else :
            try :
                y, ylab = self.getVal(vy, label=True)
                ax.set_xlabel(xlab)
                ax.set_ylabel(ylab)
                f, = ax.plot(x, y, *args, **kargs)
            except :
                args = list(args)
                args.insert(0, vy)
                f, = ax.plot(x, *args, **kargs)
                ax.set_xlabel('nb point')
                ax.set_ylabel(xlab)
        return f

    def __repr__(self):
        return self.title

    def __len__(self):
        if self._val == []:
            return -1
        else:
            return np.sum(self.accept_val) # return the size of accept_val only


class DataLoadError(Exception):
    def __init__(self, fname, val='none'):
        if val == 'header':
            print('header determination fail in {}'.format(fname))
        else:
            print('{} is not reconize as a datfile'.format(fname))



class DatFile(Dat):
    def __init__(self, fname, **kargs):
        self.dirname = os.path.dirname(fname)
        self.header = []
        if self.dirname == '':
            self.dirname = '.'
        self.basename = os.path.basename(fname)
        print(self.basename)
        if not 'title' in kargs:
            try :
                kargs['title'] = self.basename[:self.basename.rindex('.')]
            except ValueError:
                kargs['title'] = self.basename
        super().__init__(**kargs)
        # label, val, key = self._read_file()
        # self.setData(val, label, key)
        if 'load_metadata' in kargs and kargs['load_metadata']:
            self.load_metadata()

    def load(self, keepAccept_val=True):
        """ reload only the data assume label and key didn't change """
        label, val, key = self._read_file()
        self.setData(val, label, key)
        if not self.accept_val is None and keepAccept_val and not all(self.accept_val):
            accept_val = ~np.zeros(self._val[0].shape, bool)
            if accept_val.size >= self.accept_val.size :
                accept_val[:self.accept_val.size] = self.accept_val
            else:
                accept_val[:] = self.accept_val[:accept_val.size]
        else:
            accept_val = None
        self.accept_val = accept_val

    def load_metadata(self):
        """ try to load metadata if there exist """
        try:
            with open('{}/metadata.json'.format(self.dirname), 'r') as f:
                meta = json.load(f, object_hook=meta_utils.dict_to_obj)
                meta = meta[self.basename]
                self.meta.update(meta)
                print('metadata update')
        except  (FileNotFoundError, KeyError):
            print('no metadata store')
            pass
    def save(self, fname=None):
        """ save the data in the text file, 
        if fname is given this change the dirname and basename of the object then save"""
        if fname:
            self.dirname = os.path.dirname(fname)
            if self.dirname == '':
                self.dirname = '.'
            self.basename = os.path.basename(fname)
            print(f'udpate dirname to {self.dirname} and basename to {self.basename}')
        header = self.header.copy()
        header.append('\t'.join(self.label))
        np.savetxt('{}/{}'.format(self.dirname, self.basename), np.array(self._val).T, delimiter='\t', header='\n'.join(header))

    def save_metadata(self):
        """ try to load metadata if there exist """
        try:
            with open('{}/metadata.json'.format(self.dirname), 'r') as f:
                meta = json.load(f, object_hook=meta_utils.dict_to_obj)
        except FileNotFoundError:
                meta = {}
        meta.update({self.basename: self.meta})
        with open('{}/metadata.json'.format(self.dirname), 'w') as f:
            json.dump(meta, f, default=meta_utils.convert_to_dict, indent='    ')
            print('metadata update')

    def getSample(self):
        """ return the sample object associate with the basedir """
        import esample
        return esample.Sample.getFromFile(self.dirname)

    def _read_file(self):
        """ by default the readAuto methode is used, this can be overite in sub-class 
            the method shoud return a list of labels, value and keys. If keys is {} 
            or None they will be determine from the labels
        """
        fname = '{}/{}'.format(self.dirname, self.basename)
        try:
            k = HeaderParser.scanFile(fname, 20)
            if not k.success:
                raise DataLoadError(fname, 'header')
            i = k.lines
            sep = k.separator
            self.header = k.header
        except (IsADirectoryError, IndexError, UnicodeDecodeError) as e:
            raise DataLoadError(fname) from e
        print('loading of the value')
        if sep == ' ': # setting none as separator work better in case the separator is space
            sep = None
        if all([ x in ['num', 'empty'] for x in k.structure]):
            val = np.genfromtxt(fname,dtype=float, skip_header=i, delimiter=sep, unpack=True)
            if k.nbCol == 1: # force storing the data in a list
                val = [val]
            else:
                val = list(val)
        else:
            tval = np.genfromtxt(fname,dtype=None, skip_header=i, delimiter=sep)
            val = []
            for i in range(k.nbCol):
                vi = tval['f{}'.format(i)]
                if vi.dtype.name.startswith('bytes'):
                    vi = vi.astype('U')
                val.append(vi)
        return k.labels, val, k.keys

    @classmethod
    def readAuto(cls, fname, load_metadata=True, squeeze=True, **kargs):
        """ 
        fname : name of the files that will be evaluated using glob.glob
        squeeze : return the object instead of the list if only one 
        """
        fnames = glob(fname)
        if len(fnames) > 0 :
            t = [cls(x, load_metadata=load_metadata, **kargs) for x in fnames]
            print('liste of keys')
            for ti in t:
                ti.load()
                print(ti.title)
                for k,v in ti.key.items() :
                    print('{} : {}'.format(k, ti.label[v]))
            if squeeze and len(t) == 1:
                return t[0]
            else:
                return t
        else:
            return None

#TODO: transform the read_auto class to a class method of datfile
class ReadAuto(DatFile):
    def _read_file(self):
        fname = '{}/{}'.format(self.dirname, self.basename)
        try:
            k = HeaderParser.scanFile(fname, 20)
            if not k.success:
                raise DataLoadError(fname, 'header')
            i = k.lines
            sep = k.separator
            self.header = k.header
        except (IsADirectoryError, IndexError, UnicodeDecodeError) as e:
            raise DataLoadError(fname) from e
        print('loading of the value')
        if sep == ' ': # setting none as separator work better in case the separator is space
            sep = None
        if all([ x in ['num', 'empty'] for x in k.structure]):
            val = list(np.genfromtxt(fname,dtype=float, skip_header=i, delimiter=sep, unpack=True))
        else:
            val = np.genfromtxt(fname,dtype=None, skip_header=i, delimiter=sep)
            val = [val['f{}'.format(i)] for i in range(k.nbCol)]
        return k.labels, val, k.keys


class LivePlot:
    def __init__(self, ax=None, subconnect=None ):
        if ax == None :
            self.fig, self.ax = plt.subplots()
        else :
            self.ax = ax
            self.fig = ax.figure
        self.lines = []
        self.parameters = []
        self.fig.canvas.mpl_connect('key_press_event', self.onpress)
    def onpress(self, event):
        if event.key == 'e':
            self.update()
    def plot(self, t, vx, vy=None, *args, **kargs):
        u = t.plot(self.ax, vx, vy, *args, **kargs)
        self.lines.append(u)
        self.parameters.append(( t, vx, vy ))
    def update(self):
        has_update = [] 
        for u, param in zip(self.lines, self.parameters):
            t, vx, vy = param
            if not t in has_update:
                t.reload()
                has_update.append(t)
            if vy is None:
                y = t.getVal(vx)
                x = np.arange(y.size)
            else :
                x = t.getVal(vx)
                y = t.getVal(vy)
            u.set_data(x, y)
        self.fig.canvas.draw()


def splitDatfile(t, p, init_val=False):
    """
    splita Datfile object t on the list of position p
    init_val : if False position is given taken in account the mask value
    """
    if isinstance(p, int):
        p = [p]
    if isinstance(p, np.ndarray):
        p = p.tolist()
    N = len(t.accept_val)
    if not init_val:
        rg = np.arange(N)[t.accept_val]
        p = [ rg[x] for x in p]
    p.insert(0, 0) # insert the first point
    p.append(N) # insert the last point
    t_shell = copy.deepcopy(t)
    t_shell._val = None
    t_shell._accept_val = None
    nt = []
    for p1, p2 in zip(p[:-1], p[1:]):
        it = copy.deepcopy(t_shell)
        nt.append(it)
        it._val = [ x[p1:p2].copy() for x in t._val] 
        if t.hasAcceptVal():
            it.accept_val = t.accept_val[p1:p2].copy()
    return nt



def quick_show(t, vx=None):
    """ show all the graph of the Dat object 
        - t dat object
        - vx optional x axis
    """
    subplots_shape = { 1 : (1,1), 2 : (1,2), 3 : (2,2), 4 : (2,2), 5 : (2,3), 6 : (2,3), 7 : (3,3), 8 : (3,3), 9 : (3,3), 10 : (3,4), 11 : (3,4), 12 : (3,4), 13 : (4,4), 14 : (4,4), 15 : (4,4), 16 : (4,4), }
    nb_tot = len(t.key)
    i_fig = 0
    while nb_tot >0 :
        if nb_tot >16:
            nb = 16
        else:
            nb = nb_tot
        fig, axl = plt.subplots(*subplots_shape[nb])
        axl = np.asarray(axl)
        if vx:
            for k, ax in zip(list(t.key.keys())[i_fig*16:(i_fig+1)*16], axl.flat):
                t.plot(ax, vx, k, label=k)
                ax.legend()
        else:
            for k, ax in zip(list(t.key.keys())[i_fig*16:(i_fig+1)*16], axl.flat):
                t.plot(ax, k, label=k)
                ax.legend()
        fig.tight_layout()
        nb_tot -= nb
        i_fig += 1
    plt.show()


