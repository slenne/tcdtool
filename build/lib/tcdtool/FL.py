import numpy as np
import tcdtool as tl
import lmfit
import matplotlib.pyplot as plt
from scipy.signal import savgol_filter as savgol
from scipy.interpolate import InterpolatedUnivariateSpline
import matplotlib.offsetbox as offsetbox
 

def fct(x, a=1, b=0, c=0):
    z = np.zeros(x.size)
    p = x <0
    z[p] = a*x[p]+b
    z[~p] = a*x[~p]+c
    return z


def cleandata(x, y):
    """ take a list of x,y data and remove duplicate x by averaging the y value in order to do interpolation or integral"""
    p = np.argsort(x)
    x = x[p]
    y = y[p]
    prev_x = x[0]
    nx = []
    ny = []
    ybuf = []
    for xi, yi in zip(x, y):
        if xi == prev_x:
            ybuf.append(yi)
        else:
            nx.append(prev_x)
            ny.append(np.mean(ybuf))
            prev_x = xi
            ybuf = [yi]
    nx.append(prev_x)
    ny.append(np.mean(ybuf))
    return np.array(nx), np.array(ny)




def is_segment_cut_square(x1, x2, y1, y2, xs, ys):
    if x1 < xs and y1 < ys:
        return True
    if x2 < xs and y2 < ys:
        return True
    if x1 > xs and x2 > xs:
        return False
    if y1 > ys and y2 > ys:
        return False
    a = (y2-y1)/(x2-x1)
    if a > 0:
        return False
    else:
        b = y1 - x1*(y2-y1)/(x2-x1)
        return b < ys - a*xs


def is_square_fit(x, y, xs, ys):
    for x1, x2, y1, y2 in zip(x[:-1], x[1:], y[:-1], y[1:]):
        if is_segment_cut_square(x1, x2, y1, y2, xs, ys):
            return False
    return True


def size2square(s, x1, y1, x2, y2, xmax, ymax):
    xmin = s/ymax
    xtry = np.linspace(xmin, xmax, 501) #brute force need to find a better way
    ytry = s/xtry
    for xs, ys in zip(xtry, ytry):
        if is_square_fit(x1, y1, xs, ys) and is_square_fit(x2, y2, xs, ys) :
            return True, (xs, ys)
    return False, (None, None)


@lmfit.Model
def m_fit_ampl_shift(x, a=1, b=0, c=0):
    z = np.zeros(x.size)
    p = x <0
    z[p] = a*x[p]+b
    z[~p] = a*x[~p]+c
    return z
#m_fit_ampl_shift = lmfit.Model(fct)


@lmfit.Model
def atan(x, a=1, s=1, x0=0):
    return a*np.arctan(s*(x-x0))


class FL:
    def __init__(self, fi, vxy, savgol_param=0, start_zero=None, Hc_method='Nearest'):
        self.param = {}
        self.model = m_fit_ampl_shift
        self.fi = fi
        self.vxy = vxy
        self.Hc_method = Hc_method
        if start_zero is None:
            self.start_zero = np.abs(fi[0]) < 50e-3 # if the first point is lower than 50mT we consider it's 0
        else :
            self.start_zero = start_zero
        if savgol_param <= 0 : 
            savgol_param = (fi.size//20)*2+1
        self.splitFL(savgol_param)
        self.start_positive = self.getRamp1()[0][0] > 0
        # estimate the first fit param automaticaly
        self.fitFL(self.guessFitThresold())
        self.is_positive = self.param['ampl'] > 0

    def splitFL(self, savgol_param=7):
        fid = savgol(self.fi, savgol_param, 2, deriv=1 )
        p = np.where(np.signbit(fid[1:]) != np.signbit(fid[:-1]))[0]
        if len(p) > 2 :
            raise ValueError("nb of field invertion too high : {}".format(len(p)))
        if len(p) !=2 and self.start_zero or (len(p) == 2 and not self.start_zero):
            print("Warning : lengh and start_zero doesn't match, correcting start_zero")
            self.start_zero = len(p) == 2
        self.start_up = self.fi[p[-1]] < self.fi[-1]
        if self.start_zero :
            self.ramp_init = (0, p[0])
            self.ramp1 = (p[0], p[1])
            self.ramp2 = (p[1], -1)
        else:
            self.ramp_init = None
            self.ramp1 = (0, p[0])
            self.ramp2 = (p[0], -1)

    def _getPart(f):
        """ return part of the loop, val can take a value between (init, ramp1, ramp2, full)"""
        def g(self, cslope=False, cshift=False, call=False):
            if call :
                cslope = True
                cshift = True
            y = self.vxy.copy()
            if cshift:
                y -= self.param['shift']
            if cslope:
                y -=  self.fi*self.param['slope']
            return f(self, self.fi, y)
        return g

    @_getPart
    def getInit(self, fi, y):
        if self.start_zero:
            p1, p2 = self.ramp_init
            return fi[p1:p2], y[p1:p2]
        else :
            raise ValueError('there is no initial ramp for this loop')

    @_getPart
    def getRamp1(self, fi, y):
        p1, p2 = self.ramp1
        return fi[p1:p2], y[p1:p2]

    @_getPart
    def getRamp2(self, fi, y):
        p1, p2 = self.ramp2
        return fi[p1:p2], y[p1:p2]

    @_getPart
    def getFull(self, fi, y):
        p1, p2 = self.ramp2
        return fi, y

    def getRemanence(self):
        x1, y1 = self.getRamp1()
        x2, y2 = self.getRamp2()
        if 'shift' in self.param:
            y1 -= self.param['shift']
            y2 -= self.param['shift']
            print('guess remanence with shift correction')
        else :
            print('guess remanence without shift')
        p = x1.argsort()
        rem1 = np.interp(0, x1[p], y1[p])
        p = x2.argsort()
        rem2 = np.interp(0, x2[p], y2[p])
        return rem1, rem2

    def getTruncLoop(self):
        # top trunc
        fi_r1, vxy_r1 = self.getRamp1(call=True)
        switch_find = False
        for i, (i1, i2) in enumerate(zip(fi_r1[:-1], fi_r1[1:])):
            if np.signbit(i1) != np.signbit(i2) :
                if not switch_find:
                    p = i
                    switch_find = True
                else:
                    raise ValueError('Change sign occurs 2 times, top part')
        trunc_fi_r1 = fi_r1[:p+2].copy()
        trunc_vxy_r1 = vxy_r1[:p+2].copy()
        # fix the point at zero (linear interpolation)
        trunc_vxy_r1[-1] = vxy_r1[p] - fi_r1[p]*(vxy_r1[p+1]-vxy_r1[p])/(fi_r1[p+1]-fi_r1[p])
        trunc_fi_r1[-1] = 0
        # bottom trunc
        fi_r2, vxy_r2 = self.getRamp2(call=True)
        switch_find = False
        for i, (i1, i2) in enumerate(zip(vxy_r2[:-1], vxy_r2[1:])):
            if np.signbit(i1) != np.signbit(i2) :
                if not switch_find:
                    p = i
                    switch_find = True
                else:
                    raise ValueError('Change sign occurs 2 times, r2 part')
        trunc_fi_r2 = fi_r2[p:].copy()
        trunc_vxy_r2 = vxy_r2[p:].copy()
        trunc_vxy_r2[0] = 0
        trunc_fi_r2[0] = fi_r2[p] - vxy_r2[p]*(fi_r2[p+1]-fi_r2[p])/(vxy_r2[p+1]-vxy_r2[p])
        return trunc_fi_r1, trunc_vxy_r1, trunc_fi_r2, trunc_vxy_r2

    def find_largest_fit(self, error=2e-14, vtry_max=0):
        x1, y1, x2, y2 = self.getTruncLoop()
        if not self.is_positive: # the loop area is compute only for one sign
            y1 *= -1
            y2 *= -1
        xmax, ymax = ( max(x1.max(), x2.max()), max(y1.max(), y2.max()))
        if vtry_max == 0:
            vtry_max = xmax*ymax
        vtry_min = 0
        sol = (0, 0)
        i = 0
        while vtry_max-vtry_min > error:
            i += 1
            ntry = (vtry_max+vtry_min)/2
            out, data = size2square(ntry, x1, y1, x2, y2, xmax, ymax)
            if out:
                print('test {}'.format(i))
                vtry_min = ntry
                sol = data
            else:
                print('test fail {}'.format(i))
                vtry_max = ntry
        print("find largest square in {} run".format(i))
        return sol, vtry_min


    def getIntegrateArea(self, ax=None):
        """ Integrate the uper square area"""
        # get the data
        x1, y1, x2, y2 = self.getTruncLoop()
        if not self.is_positive: # the loop area is compute only for one sign
            y1 *= -1
            y2 *= -1
        # ensure there is no 2 y value for 1 x value witch fail interpolation
        x1, y1 = cleandata(x1, y1)
        x2, y2 = cleandata(x2, y2)
        x2 = np.insert(x2, 0, 0)
        y2 = np.insert(y2, 0, 0)
        xmax = min(x1[-1], x2[-1])
        # remove point for H<0
        p = x1 >= 0
        x1, y1 = x1[p], y1[p]
        p = x2 >= 0
        x2, y2 = x2[p], y2[p]
        y1[y1<0] = 0
        y2[y2<0] = 0
        # interpolate the value
        f1 = InterpolatedUnivariateSpline(x1, y1, k=1)
        f2 = InterpolatedUnivariateSpline(x2, y2, k=1)
        area = np.abs(f1.integral(0, xmax)- f2.integral(0, xmax))
        if ax is not None:
            xev = np.linspace(0, xmax, 600)
            if self.is_positive: # plot correctly
                ax.fill_between(xev, f1(xev), f2(xev), color='#66ff33')
            else:
                ax.fill_between(xev, -f1(xev), -f2(xev), color='#66ff33')
        return area

    def getSquerness(self, plot=False):
        if plot:
            #import matplotlib.pyplot as plt
            import matplotlib.patches as mpl_patches
            #import matplotlib.offsetbox as offsetbox
            fig, ax = plt.subplots()
            x, y = self.getRamp1(call=True)
            ax.plot(x, y, '-+', color='black', label='top')
            x, y = self.getRamp2(call=True)
            ax.plot(x, y, '-+', color='blue', label='bottom')
        else:
            ax = None
        area_full = self.getIntegrateArea(ax)
        square, area_sq = self.find_largest_fit(error=area_full/1000)
        if plot:
            xs, ys = square
            if self.is_positive:
                i = mpl_patches.Rectangle((0, 0), xs, ys, color='red')
            else :
                i = mpl_patches.Rectangle((0, 0), xs, -ys, color='red')
            ax.add_patch(i)
            if self.is_positive:
                loc = 2
            else:
                loc = 1
            text = offsetbox.AnchoredText('area full = {:.2e}\narea square = {:.2e}\nratio = {:.1f} %'.format(area_full, area_sq, area_sq/area_full*100), loc=loc)
            ax.add_artist(text)
            plt.show()
        return area_full, area_sq

    @staticmethod
    def getHcNearest(x, y):
        """ Linear approximation from nearest point crossing zeros"""
        pl = np.argmin(np.abs(y))
        # if y[pl] < 0:
            # if y[pl+1] >0 :
                # pl1 = pl+1
            # else :
                # pl1 = pl-1
        # else :
            # if y[pl+1] <0 :
                # pl1 = pl+1
            # else :
                # pl1 = pl-1
        if  (y[pl] < 0) ^ (y[pl+1] <0):
            pl1 = pl+1
        else:
            pl1 = pl-1
        hc = x[pl]-y[pl]*(x[pl1]-x[pl])/(y[pl1]-y[pl])
        return hc

    @staticmethod
    def getHcAtan(x, y):
        """ fit with an arctan function to get the Hc Value """
        out = atan.fit(y, x=x)
        return out.params['x0'].value

    def getHc(self, x, y, method=None):
        if method == None:
            method = self.Hc_method
        if method == 'Nearest':
            return self.getHcNearest(x, y)
        elif method == 'Atan':
            return self.getHcAtan(x, y)
    
    def guessFitThresold(self):
        r1, r2 = self.getRemanence()
        sh = (r1+r2)/2
        p = self.fi<0
        a = self.getHcNearest(self.fi[p], self.vxy[p]-sh)
        p = self.fi>0
        b = self.getHcNearest(self.fi[p], self.vxy[p]-sh)
        x1 = (3*self.fi.max()+2*b)/5
        x2 = (3*self.fi.min()+2*a)/5
        return (self.fi>x1) | (self.fi<x2)

    def fitFL(self, plist):
        self.using_val = plist #record for later
        out = self.model.fit(self.vxy[plist], None, x=self.fi[plist] )
        self.FL_fit_out = out
        a, b, c = self.FL_fit_out.values['a'], self.FL_fit_out.values['b'], self.FL_fit_out.values['c']
        ampl = c-b
        shift = (c+b)/2
        self.param.update({  'slope' : a,
                          'ampl' : ampl,
                          'shift' : shift,
                          })
        hc_1_slope = self.getHc(*self.getRamp1(call=True))
        hc_2_slope = self.getHc(*self.getRamp2(call=True))
        # try:
            # hc_pos, hc_neg = sorted([hc_1_slope, hc_2_slope])
            # p = (self.fi > 1.2*hc_pos) | (self.fi < 1.2*hc_neg)
            # hc_neg, hc_pos = self.getHc(self.fi[p], self.vxy[p]-shift)
        # except IndexError:
            # hc_neg, hc_pos = np.nan, np.nan
        self.param.update({ 'hc_1_slope_correction' : hc_1_slope,
                          'hc_2_slope_correction' : hc_2_slope,
                          })
        rem1, rem2 = self.getRemanence()
        self.param['rem1'] = rem1
        self.param['rem2'] = rem2
        #self.fitcurve.set_data(self.x_eval, self.model.eval(out.params, x=self.x_eval))
    def fitFlGui(self):
        fig, ax = plt.subplots()
        u, = ax.plot(self.fi, self.vxy)
        k = FLGui(self, ax, u, preselect=self.using_val)
        plt.show()

    def plotFLfit(self, include_shift=False, ax=None):
        """ plot the diffrent value on a graph
        method in order to save it later
        include_shift allow to shift or not the plot 
        ax : optional
        """
        x_eval = np.linspace(self.fi.min(), self.fi.max(), 201)
        if ax is None:
            fig, ax = plt.subplots()
        if self.is_positive:
            loc =1
        else :
            loc = 2
        if include_shift:
            shift = self.param['shift']
        else:
            shift = 0
        slope = self.param['slope']
        ampl = self.param['ampl']
        hc_neg = self.param['hc_1_slope_correction']
        hc_pos = self.param['hc_2_slope_correction']
        ampl_at0_1 = self.param['rem1']
        ampl_at0_2 = self.param['rem2']
        legend = "slope : {:.4e}\nampl : {:.4e}\nremanence : {:.4e}\nshift : {:.4e}\nhc_neg : {:.4e}\nhc_pos : {:.4e}".format(slope, ampl, ampl_at0_1, self.param['shift'], hc_neg, hc_pos)
        ax.plot(self.fi, self.vxy-shift, '-+')
        ax.plot(x_eval, m_fit_ampl_shift.func(x_eval, slope, self.param['shift']-shift-ampl/2, self.param['shift']-shift+ampl/2), '-')
        ax.plot([0,0], [self.param['shift']-shift+ampl_at0_1, self.param['shift']-shift+ampl_at0_2], 'og')
        ax.axvline(x=hc_neg, linestyle='--', color='r')
        ax.axvline(x=hc_pos, linestyle='--', color='r')
        text = offsetbox.AnchoredText(legend, loc=loc)
        ax.add_artist(text)   

    def fromAx(self, ax, u, param=None):
        """ constructeur that behave like the old getAmplHcshift """
        pass


class FLGui(tl.IncrSelect):
    def __init__(self, FL, ax, line, **kargs):
        self.FL = FL
        self.fitcurve, = ax.plot([], [], '-', scaley=False)
        self.remanence, = ax.plot([], [], 'og', scaley=False)
        self.lhc_neg = ax.axvline(x=np.nan, linestyle='--', color='r')
        self.lhc_pos = ax.axvline(x=np.nan, linestyle='--', color='r')
        super().__init__(ax, line, **kargs)
        ax.figure.canvas.mpl_connect('key_press_event', self.onPress)
        self.x_eval = np.linspace(self.x[0].min(), self.x[0].max(), 201)
        p = self.y[0].argmax()
        if self.x[0][p] < 0:
            loc =1
        else :
            loc = 2
        self.text = offsetbox.AnchoredText('', loc=loc)
        ax.add_artist(self.text)   
        if 'preselect' in kargs:
            self.process()
            self.redraw()

    def symetrise(self):
        """ Symetrise the limit for the field """
        vmin = np.min(self.x[0][~self.using_val.squeeze()])
        vmax = np.max(self.x[0][~self.using_val.squeeze()])
        vnew = (vmax-vmin)/2
        self.using_val[0,:] = np.logical_or(self.x[0] > vnew, self.x[0] < -vnew)

    def onPress(self, event):
        """ connect space to symetrise """
        if event.key == ' ':
            self.symetrise()
            self.process()
            self.redraw()

    def after_select(self, eclick, erelease):
        self.process()

    def process(self):
        self.FL.fitFL(self.using_val.squeeze())
        shift = self.FL.param['shift']
        self.fitcurve.set_data(self.x_eval, m_fit_ampl_shift.func(self.x_eval, self.FL.param['slope'], shift-self.FL.param['ampl']/2, shift+self.FL.param['ampl']/2))
        self.remanence.set_data([0,0], [shift+self.FL.param['rem1'], shift+self.FL.param['rem2']])
        self.lhc_neg.set_xdata([self.FL.param['hc_neg_slope_correction'], self.FL.param['hc_neg_slope_correction']])
        self.lhc_pos.set_xdata([self.FL.param['hc_pos_slope_correction'], self.FL.param['hc_pos_slope_correction']])
        legend = "slope : {:.4e}\nampl : {:.4e}\nremanence : {:.4e}\nshift : {:.4e}\nhc_neg : {:.4e}\nhc_pos : {:.4e}".format(self.FL.param['slope'], self.FL.param['ampl'], self.FL.param['rem1'], shift, self.FL.param['hc_neg_slope_correction'], self.FL.param['hc_pos_slope_correction'])
        self.text.txt.set_text(legend)

