import re

#-*-

comment = '#'
separators = ' ,;\t'
s_alpha_num = '[\w0-9:\.\-\\/=>< ]+'
s_num = '^[-+]?\d+(?:\.\d+)?(?:[eE][-+]?\d+)?'
exp_number = re.compile(s_num)
alpha_num = re.compile(s_alpha_num)
commented = re.compile('^ *[{}]'.format(comment))


def cleanSep(sep):
    """ take a list of separators and return the correct ones """
    is_space = all([ x == ' ' for x in sep])
    if is_space:
        return True, ' '
    else :
        sep = [x for x in sep if x != ' ']
        is_consistant = all([ sep[0] == x for x in sep])
        if is_consistant :
            return True, sep[0]
        else:
            return False, ''




def identify(st):
    """ return if the string is alpha_num or num """
    if st == '':
        return 'empty'
    elif exp_number.fullmatch(st):
        return 'num'
    elif alpha_num.fullmatch(st) :
        return 'alpha_num'
    else:
        return ''



class HeaderParser:
    def __init__(self):
        self.separator = ','
        self.structure = []
        self.compiler = None
        self.trailing_sep = False
        self.header = []
        self.success = False
    def compile(self):
        """ return an object to parse faster the next line """
        if self.structure == []:
            self.compiler = None
        elif all([ x in ['alpha_num', 'empty']  for x in self.structure ]):
            self.compiler = None
        else:
            s = ' *'
            for x in self.structure:
                if x == 'alpha_num':
                    s += s_alpha_num
                elif x == 'num':
                    s += s_num
                s += ' *{} *'.format(self.separator)
            self.compiler = re.compile(s)

    def findStructure(self, st):
        """ take a string in input (st) and try to bet the structure. return the status of the detection (success or fail)
        and the structure and sep """
        #TODO: might be better to raise an error if the structure fail, or empty structure, lets see
        buff = ''
        structure = []
        skip_space = False
        sep = []
        n = len(st)
        i = 0
        while i < n:
            c = st[i]
            if c in separators and not (skip_space and c == ' ') :
                sep.append(c)
                tp = identify(buff.strip())
                buff = ''
                if tp == '':
                    return False, structure, ''
                elif tp != 'empty' or c != ' ' :
                    structure.append(tp)
                if not skip_space and c != ' ': # restart the loop with the skip_space option
                    i = -1 # will be cancel by the +1 bellow
                    structure = []
                    skip_space = True
                    sep = []
                    buff = ''
            else:
                buff += c
            i += 1
        # Treat the last case if it's doesn't end by a separtors
        sep_stat, sep = cleanSep(sep)  # the status of the findStructure is True if the code run up to here
        buff = buff.strip()
        tp = identify(buff)
        if tp == '':
            return False, structure, ''
        else :
            structure.append(tp)
        return sep_stat, structure, sep
    def structureUpdate(self, struct):
        """ update a structure from the memory one. and determine if the success is present"""
        success = True
        if self.structure == [] or len(self.structure) != len(struct):
            self.structure = struct
            success = False
        else:
            for i,(vo, vn) in enumerate(zip(self.structure, struct)):
                if vo != vn:
                    if vo == 'empty' or (vo == 'num' and vn == 'alpha_num'):
                        self.structure[i] = vn
                    elif vn == 'empty':
                        pass
                    else:
                        success = False
                        self.structure[i] = vn
        self.compile()
        return success
    def processLine(self, st):
        """ process a line and return if the status
            pass = 1
            skip = 0
            fail = -1
        """
        if commented.match(st):
            return 0
        if self.compiler and self.compiler.fullmatch(st):
            return 1
        else:
            st, struct, self.separator = self.findStructure(st)
            if not st:
                return -1
            else:
                if self.structureUpdate(struct):
                    return 1
                else:
                    return -1
    @property
    def nbCol(self):
        return len(self.structure)

    #TODO: move this class to the init
    @classmethod
    def scanFile(cls, fname, match_sucess=5):
        k = cls()
        buff_header = []
        count_success = 0
        with open(fname, 'r') as f:
            for st in f:
                st = st.strip(' \n')
                res = k.processLine(st)
                if res <0:
                    k.success = False
                    count_success = 0
                    k.header.extend(buff_header)
                    buff_header = []
                else:
                    count_success += res
                    if res >0:
                        k.success = True
                if count_success > match_sucess and ( k.nbCol >1 or not k.structure[0] == 'alpha_num')  : # force continue read the full file if only one colon is find
                    break
                if not k.success and res == 0:
                    k.header.append(st)
                else:
                    buff_header.append(st)
        k.lines = len(k.header)
        k.buildLabel()
        k.cleanHeader()
        return k
    def cleanHeader(self):
        self.header = [ x.lstrip('# ') for x in self.header]

    def buildLabel(self):
        default = True
        if len(self.header) >0:
            st = re.sub(f'^[{comment} ]*', '', self.header[-1].strip(' '))
            if self.separator != ' ':
                label = [ x.strip() for x in  st.split(self.separator) ]
            else :
                label = [ x for x in  st.split(self.separator) if x != '' ]
            if len(label) == self.nbCol :
                key = get_key(label)
                default = False
                self.header.pop(-1)
        if default:
            label = [ 'label {}'.format(i) for i in range(self.nbCol)]
            key = {}
        self.labels, self.keys = label, key






def get_key(label):
    val = []
    for i,v in enumerate(label):
        v = label2key(v)
        if not v in val:
            val.append(v)
        else :
            val.append(v+str(i))
    key = { v : i for i,v in enumerate(val)}
    return key


def label2key(label):
    """ return the key generate from the label """
    key = re.sub('[{}]'.format(comment), '', label).strip()
    key = re.sub('[\[\(].*', '', key).strip()
    key = re.sub(' +', '_', key)
    return key



#TODO idea upgrade procsse line by
# line, when match compile an expression for checking
# the next line
# include also, boolean and alphanumeric data
#  class HearderParser:
#      def __init__(self):
#          self.line = 0
#          self.success = False
#          self.sep = ''
#          self.nbcol = -1
#          self.header = []
#      @property
#      def lines(self):
#          return len(self.header)
#      def inferLabel(self):
#          pass
#
#      def buildLabel(self):
#          s2 = self.header[-1]
#          u = [ re.sub('^{0}|{0}$'.format(separators), '', x) for x in  s2.split(self.sep) ]
#          if len(u) == self.nbcol :
#              label = [ x.strip() for x in u]
#              key = get_key(label)
#              self.header.pop(-1)
#          else:
#              label = [ 'label {}'.format(i) for i in range(self.nbcol)]
#              key = {}
#          return label, key
#
#
#
#



def get_header(fname):
    k = HearderParser.scanFile(fname, 20)
    return label, skipline, header, key, sep

