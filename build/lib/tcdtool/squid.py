import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import matplotlib.offsetbox as offsetbox

def cgs2metric(t):
    """ convert squid data from cgs to metric 
    t is a squid dat file"""
    fi = t.getVal('Field')*1e-4
    t.add(fi, 'Field (T)', 'Field')
    lm = t.getVal('Long_Moment')/1e6
    t.add(lm, 'Long Moment (kA m^2)', 'Long_Moment')

class SlopeCorrector:
    @staticmethod
    def fct_slope(x, a=1, b=0, c=0):
        z = np.zeros(x.size)
        p = x <0
        z[p] = a*x[p]+b
        z[~p] = a*x[~p]+c
        return z
    def __init__(self, x, y, start=None, stop=None):
        """ x,y are the data, start is  field min to fit and stop is the field max"""
        self.x, self.y = x, y
        if start == None:
            self.mn = x.min()*3/5
        else:
            self.mn = start
        if stop == None:
            self.mx = x.max()*3/5
        else:
            self.mx= stop
    def loadFromReport(self, rep):
        """ took the rep generate from the report method and re-insert the start stop value"""
        self.mn = rep['H_low']
        self.mx = rep['H_high']
    def run(self):
        """ run the fit left click to change the fit line"""
        self.fig, ax = plt.subplots()
        self.line_min = ax.axvline(self.mn, ls='--', color='black')
        self.line_max = ax.axvline(self.mx, ls='--', color='black')
        self.text = offsetbox.AnchoredText('', loc=1)
        ax.add_artist(self.text)   
        fy = self.fit()
        ax.plot(self.x, self.y)
        self.l_fit, = ax.plot(self.x, fy, '-r')
        self.fig.canvas.mpl_connect('button_press_event', self.click_ev)
        self.update_label()
        plt.show()
    def click_ev(self, event):
        if event.button == 1:
            val=event.xdata
            if val <0:
                self.line_min.set_data([val, val], [0, 1])
                self.mn = val
            else :
                self.line_max.set_data([val, val], [0, 1])
                self.mx = val
            fy = self.fit()
            self.l_fit.set_data(self.x, fy)
            self.update_label()
            self.fig.canvas.draw_idle()
    def fit(self):
        p = np.logical_or(self.x<self.mn, self.x>self.mx)
        self.opt, self.covar = curve_fit(SlopeCorrector.fct_slope, self.x[p], self.y[p])
        stot = np.sum((self.y[p] - self.y[p].mean())**2)
        sreg = np.sum((SlopeCorrector.fct_slope(self.x[p], *self.opt)- self.y[p].mean())**2)
        self.Rsquared = sreg/stot
        return SlopeCorrector.fct_slope(self.x, *self.opt)
    def update_label(self):
        rep = self.report()
        t = 'slope = {:.2e}\nampl = {:.2e}\nshift = {:.2e}\nR^2 = {:.3e}'.format(rep['param'][0], rep['param'][1], rep['param'][2], rep['R^2'] )
        self.text.txt.set_text(t)
    def report(self):
        """return a dict with all the information about the fit"""
        v = {
                'H_low': self.mn,
                'H_high': self.mx,
                'param': self.opt.tolist(),
                'covar': self.covar.tolist(),
                'R^2': self.Rsquared,
                }
        return v


