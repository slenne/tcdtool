import numpy as np
from scipy.signal import savgol_filter as savgol
from scipy.interpolate import InterpolatedUnivariateSpline
 

def fct(x, a=1, b=0, c=0):
    z = np.zeros(x.size)
    p = x <0
    z[p] = a*x[p]+b
    z[~p] = a*x[~p]+c
    return z


def cleandata(x, y):
    """ take a list of x,y data and remove duplicate x by averaging the y value in order to do interpolation or integral"""
    p = np.argsort(x)
    x = x[p]
    y = y[p]
    prev_x = x[0]
    nx = []
    ny = []
    ybuf = []
    for xi, yi in zip(x, y):
        if xi == prev_x:
            ybuf.append(yi)
        else:
            nx.append(prev_x)
            ny.append(np.mean(ybuf))
            prev_x = xi
            ybuf = [yi]
    nx.append(prev_x)
    ny.append(np.mean(ybuf))
    return np.array(nx), np.array(ny)




def is_segment_cut_square(x1, x2, y1, y2, xs, ys):
    if x1 < xs and y1 < ys:
        return True
    if x2 < xs and y2 < ys:
        return True
    if x1 > xs and x2 > xs:
        return False
    if y1 > ys and y2 > ys:
        return False
    b = y1 - x1*(y2-y1)/(x2-x1)
    a = (y2-y1)/(x2-x1)
    if a > 0:
        return False
    else:
        return b < ys - a*xs


def is_square_fit(x, y, xs, ys):
    for x1, x2, y1, y2 in zip(x[:-1], x[1:], y[:-1], y[1:]):
        if is_segment_cut_square(x1, x2, y1, y2, xs, ys):
            return False
    return True


def size2square(s, x1, y1, x2, y2, xmax, ymax):
    xmin = s/ymax
    xtry = np.linspace(xmin, xmax, 51) #brute force need to find a better way
    ytry = s/xtry
    for xs, ys in zip(xtry, ytry):
        if is_square_fit(x1, y1, xs, ys) and is_square_fit(x2, y2, xs, ys) :
            return True, (xs, ys)
    return False, (None, None)



class FL:
    def __init__(self, fi, vxy, savgol_param=0):
        self.fi = fi
        self.vxy = vxy
        self.start_zero = np.abs(fi[0]) < 50e-3 # if the first point is lower than 50mT we consider it's 0
        self.is_positive = self.vxy[self.fi.argmax()] > 0
        if savgol_param <= 0 : 
            savgol_param = (fi.size//20)*2+1
        self.splitFL(savgol_param)

    def splitFL(self, savgol_param=7):
        fid = savgol(self.fi, savgol_param, 2, deriv=1 )
        p = np.where(np.signbit(fid[1:]) != np.signbit(fid[:-1]))[0]
        if len(p) > 2 :
            raise ValueError("nb of field invertion too high : {}".format(len(p)))
        if len(p) !=2 and self.start_zero or (len(p) == 2 and not self.start_zero):
            print("Warning : lengh and start_zero doesn't match")
        self.start_up = self.fi[p[-1]] < self.fi[-1]
        if self.start_zero :
            self.ramp_init = (0, p[0])
            self.ramp1 = (p[0], p[1])
            self.ramp2 = (p[1], -1)
        else:
            self.ramp_init = None
            self.ramp1 = (0, p[0])
            self.ramp2 = (p[0], -1)

    def getPart(self, val):
        """ return part of the loop, val can take a value between (init, ramp1, ramp2, full)"""
        if val == 'init':
            if self.start_zero:
                p1, p2 = self.ramp_init
                return self.fi[p1:p2], self.vxy[p1:p2]
            else :
                raise ValueError('there is no initial ramp for this loop')
        elif val == 'ramp1':
            p1, p2 = self.ramp1
            return self.fi[p1:p2], self.vxy[p1:p2]
        elif val == 'ramp2':
            p1, p2 = self.ramp2
            return self.fi[p1:p2], self.vxy[p1:p2]
        elif val == 'full':
            return self.fi, self.vxy
        else:
            raise ValueError('the argument should be one of: (init, ramp1, ramp2, full)')

    def getTruncLoop(self):
        # top trunc
        fi_r1, vxy_r1 = self.getPart('ramp1')
        switch_find = False
        for i, (i1, i2) in enumerate(zip(fi_r1[:-1], fi_r1[1:])):
            if np.signbit(i1) != np.signbit(i2) :
                if not switch_find:
                    p = i
                    switch_find = True
                else:
                    raise ValueError('Change sign occurs 2 times, top part')
        trunc_fi_r1 = fi_r1[:p+2].copy()
        trunc_vxy_r1 = vxy_r1[:p+2].copy()
        # fix the point at zero (linear interpolation)
        trunc_vxy_r1[-1] = vxy_r1[p] - fi_r1[p]*(vxy_r1[p+1]-vxy_r1[p])/(fi_r1[p+1]-fi_r1[p])
        trunc_fi_r1[-1] = 0
        # bottom trunc
        fi_r2, vxy_r2 = self.getPart('ramp2')
        switch_find = False
        for i, (i1, i2) in enumerate(zip(vxy_r2[:-1], vxy_r2[1:])):
            if np.signbit(i1) != np.signbit(i2) :
                if not switch_find:
                    p = i
                    switch_find = True
                else:
                    raise ValueError('Change sign occurs 2 times, r2 part')
        trunc_fi_r2 = fi_r2[p:].copy()
        trunc_vxy_r2 = vxy_r2[p:].copy()
        trunc_vxy_r2[0] = 0
        trunc_fi_r2[0] = fi_r2[p] - vxy_r2[p]*(fi_r2[p+1]-fi_r2[p])/(vxy_r2[p+1]-vxy_r2[p])
        return trunc_fi_r1, trunc_vxy_r1, trunc_fi_r2, trunc_vxy_r2

    def find_largest_fit(self, error=2e-14):
        x1, y1, x2, y2 = self.getTruncLoop()
        if not self.is_positive: # the loop area is compute only for one sign
            y1 *= -1
            y2 *= -1
        xmax, ymax = ( max(x1.max(), x2.max()), max(y1.max(), y2.max()))
        vtry_max = xmax*ymax
        vtry_min = 0
        sol = (0, 0)
        i = 0
        while vtry_max-vtry_min > error:
            i += 1
            ntry = (vtry_max+vtry_min)/2
            out, data = size2square(ntry, x1, y1, x2, y2, xmax, ymax)
            if out:
                print('test {}'.format(i))
                vtry_min = ntry
                sol = data
            else:
                print('test fail {}'.format(i))
                vtry_max = ntry
        print("find largest square in {} run".format(i))
        return sol, vtry_min

    def integratedArea(self):
        x1, y1 = self.getPart('ramp1')
        x2, y2 = self.getPart('ramp2')

    def getIntegrateArea(self, ax=None):
        """ Integrate the uper square area"""
        # get the data
        x1, y1, x2, y2 = self.getTruncLoop()
        if not self.is_positive: # the loop area is compute only for one sign
            y1 *= -1
            y2 *= -1
        # ensure there is no 2 y value for 1 x value witch fail interpolation
        x1, y1 = cleandata(x1, y1)
        x2, y2 = cleandata(x2, y2)
        x2 = np.insert(x2, 0, 0)
        y2 = np.insert(y2, 0, 0)
        xmax = min(x1[-1], x2[-1])
        # remove point for H<0
        p = x1 >= 0
        x1, y1 = x1[p], y1[p]
        p = x2 >= 0
        x2, y2 = x2[p], y2[p]
        y1[y1<0] = 0
        y2[y2<0] = 0
        # interpolate the value
        f1 = InterpolatedUnivariateSpline(x1, y1, k=1)
        f2 = InterpolatedUnivariateSpline(x2, y2, k=1)
        area = np.abs(f1.integral(0, xmax)- f2.integral(0, xmax))
        if ax is not None:
            xev = np.linspace(0, xmax, 600)
            if self.is_positive: # plot correctly
                ax.fill_between(xev, f1(xev), f2(xev), color='#66ff33')
            else:
                ax.fill_between(xev, -f1(xev), -f2(xev), color='#66ff33')
        return area

    def getSquerness(self, plot=False):
        if plot:
            import matplotlib.pyplot as plt
            import matplotlib.patches as mpl_patches
            import matplotlib.offsetbox as offsetbox
            fig, ax = plt.subplots()
            x, y = self.getPart('ramp1')
            ax.plot(x, y, '-+', color='black', label='top')
            x, y = self.getPart('ramp2')
            ax.plot(x, y, '-+', color='blue', label='bottom')
        else:
            ax = None
        area_full = self.getIntegrateArea(ax)
        square, area_sq = self.find_largest_fit(error=area_full/1000)
        if plot:
            xs, ys = square
            if self.is_positive:
                i = mpl_patches.Rectangle((0, 0), xs, ys, color='red')
            else :
                i = mpl_patches.Rectangle((0, 0), xs, -ys, color='red')
            ax.add_patch(i)
            if self.is_positive:
                loc = 2
            else:
                loc = 1
            text = offsetbox.AnchoredText('area full = {:.2e}\narea square = {:.2e}\nratio = {:.1f} %'.format(area_full, area_sq, area_sq/area_full*100), loc=loc)
            ax.add_artist(text)
            plt.show()
        return area_full, area_sq

