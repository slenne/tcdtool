#!/usr/bin/env python

import time
import json
import numpy as np


manual_serial = []

# objet to be store in meta and have more general property

class Tcomp:
    def __init__(self, value, quality='equal', error=None):
        if not quality in ['equal', 'superior', 'lower']:
            raise ValueError('quality argument take value [equal, superior, lower]')
        self.value = value
        self.error = error
        self.quality = quality
        self.timestamp = int(time.time())
        self.origin = ''
    def show(self, verbose=0):
        st = {'equal' : '=', 'lower': '<', 'superior':'>'}
        if self.error is None:
            m_out = 'Tcomp {} {} K'.format(st[self.quality], self.value)
        else:
            m_out = 'Tcomp {} {} ± {} K'.format(st[self.quality], self.value, self.error)
        if verbose == 0:
            out = m_out
        elif verbose >0:
            dt = 'record : {}'.format(time.strftime("%D %H:%M", time.localtime(self.timestamp)))
            fr = 'from : {}'.format(self.origin)
            out = '{}\n{}\n{}'.format(m_out, fr, dt)
        return out
    @classmethod
    def load(cls, arg):
        self = cls.__new__(cls)
        self.__dict__.update(arg)
        return self
    def __repr__(self):
        return self.show(verbose=0)



manual_serial.append(Tcomp)





def convert_to_dict(obj):
  """
  A function takes in a custom object and returns a dictionary representation of the object.
  This dict representation includes meta data such as the object's module and class names.
  """
  if isinstance(obj, np.ndarray):
      obj_dict = {
      "__class__": obj.__class__.__name__,
      "data" :  obj.tolist(),
      }
      return obj_dict
  elif isinstance(obj, manual_serial):
      #  Populate the dictionary with object meta data 
      obj_dict = {
        "__class__": obj.__class__.__name__,
        "__module__": obj.__module__
      }
      
      #  Populate the dictionary with object properties
      obj_dict.update(obj.__dict__)
      
      return obj_dict
  else:
      return obj




def dict_to_obj(our_dict):
    """
    Function that takes in a dict and returns a custom object associated with the dict.
    This function makes use of the "__module__" and "__class__" metadata in the dictionary
    to know which object type to create.
    """
    if "__class__" in our_dict:
        if our_dict["__class__"] == 'ndarray':
            obj = np.array(our_dict['data'])
        else:
            class_name = our_dict.pop("__class__")
            module_name = our_dict.pop("__module__")
            module = __import__(module_name, fromlist=[''])
            class_ = getattr(module, class_name)
            obj = class_.load(our_dict)
    else:
        obj = our_dict
    return obj


manual_serial = tuple(manual_serial)

