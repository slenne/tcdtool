import numpy as np
import matplotlib.pyplot as plt
import matplotlib.offsetbox as offsetbox
from matplotlib.widgets import RectangleSelector
import re
import matplotlib as mpl

class Select:
    def __init__(self, ax, line, reverse=False, callback=None):
        """ 
        ax : axe mpl ou est tracer la figure
            line : 
            """
        self.callback = callback
        self.reverse = reverse
        if hasattr(line, '__iter__'):
            nb_line = len(line)
            self.x = []
            self.y = []
            for li in line:
                x = li.get_xdata()
                y = li.get_ydata()
                p = np.logical_and(np.isfinite(x), np.isfinite(y))
                self.x.append(x[p])
                self.y.append(y[p])
            # if len(set([len(i) for i in self.x])) != 1:
                # raise ValueError('Different size for the list of point. check that there is not different number of NaN in the input data.')
        else:
            nb_line = None
            x = line.get_xdata()
            y = line.get_ydata()
            p = np.logical_and(np.isfinite(x), np.isfinite(y))
            self.x = [x[p]]
            self.y = [y[p]]
        
        # on connect l'axe a la fonction onselect
        if mpl.__version__[0] == 2:
            self.selector = RectangleSelector(ax, self._onselect,
                    drawtype='box', useblit=True,
                    button=[1, 3],  # don't use middle button
                    spancoords='pixels',
                    interactive=False)
        else:
            # the RectangleSelector change the axis scale, so we save and reload the axis. Seemed fix in 3.7
            xlim = ax.get_xlim()
            ylim = ax.get_ylim()
            self.selector = RectangleSelector(ax, self._onselect)
            ax.set_xlim(xlim)
            ax.set_ylim(ylim)

    def select(self, eclick, erelease):
        x1, y1 = eclick.xdata, eclick.ydata
        x2, y2 = erelease.xdata, erelease.ydata
        xmin, xmax = np.sort([x1, x2])
        ymin, ymax = np.sort([y1, y2])
        ps = []
        for xi, yi in zip(self.x, self.y):
            if self.reverse :
                logic_x = np.logical_or(xi<xmin, xi>xmax)
                logic_y = np.logical_or(yi<ymin, yi>ymax)
            else :
                logic_x = np.logical_and(xi>xmin, xi<xmax)
                logic_y = np.logical_and(yi>ymin, yi<ymax)
            p = np.where(np.logical_and(logic_x, logic_y))
            ps.append(p)
        # if len(ps) == 1 :
            # ps = ps[0]
        return ps

    def _onselect(self, eclick, erelease):
        self.onselect(eclick, erelease)
        if not self.callback is None:
            self.callback()

    def onselect(self, eclick, erelease):
        x1, y1 = eclick.xdata, eclick.ydata
        x2, y2 = erelease.xdata, erelease.ydata
        print('------')
        print(x1, y1)
        print(x2, y2)
        print('\n')
        print(self.select(eclick,erelease))
        print('------\n')
        return self.select(eclick,erelease)



class IncrSelect(Select):
    """ filtre graphiquement les objet Dat, voir Dat.filter """
    def __init__(self, ax, line, preselect=None, fullscale=True, **kargs):
        if hasattr(line, '__iter__'):
            self.line = line
        else:
            self.line = [line]
        self.err = []
        for li in self.line:
            u, = ax.plot([], [], '.', color=li.get_color(), alpha=0.6, scaley=False)
            self.err.append(u)
        super().__init__(ax, line, reverse=False, **kargs) 
        self.using_val = np.ones((len(self.line), *self.x[0].shape), bool)
        if preselect is not None:
            if preselect.ndim == 1:
                preselect = preselect.reshape(1, -1)
            if preselect.shape != self.using_val.shape :
                raise ValueError("preselect size doesn't match data shape")
            self.using_val = preselect.reshape(1,-1)
            self.redraw()
            if not fullscale:
                xmin = np.nan
                xmax = np.nan
                ymin = np.nan
                ymax = np.nan
                for xi, yi, pi in zip(self.x, self.y, self.using_val):
                    xmin = np.nanmin([xmin, np.min(xi[pi])]) # no need of the second nanmin only finite data are considered in Select object
                    ymin = np.nanmin([ymin, np.min(yi[pi])])
                    xmax = np.nanmax([xmax, np.max(xi[pi])])
                    ymax = np.nanmax([ymax, np.max(yi[pi])])
                ax.set_xlim(xmin-(xmax-xmin)*0.05, xmax+(xmax-xmin)*0.05)
                ax.set_ylim(ymin-(ymax-ymin)*0.05, ymax+(ymax-ymin)*0.05)

        # select.__init__ should be after the deff of self.err otherwise we got
        # wrong scale I don't know why...
    def onselect(self, eclick, erelease):
        p = self.select(eclick, erelease)
        if eclick.button == 1:
            self.update(False, p)
        else:
            self.update(True, p)
        self.redraw()
        self.after_select(eclick, erelease)

    def update(self, val, p):
        #if len(p) == len(self.using_val):
        for i, pi in enumerate(p):
            self.using_val[i,pi] = val
        # else:
            # self.using_val[:, p] = val

    def redraw(self):
        for li, erri, xi, yi, pi in zip(self.line, self.err, self.x, self.y, self.using_val):
            li.set_data(xi[pi], yi[pi])
            erri.set_data(xi[~pi], yi[~pi])
        plt.draw()

    def mergeVal(self, logic=None):
        """ return the merge using val value """
        if logic is None:
            return np.logical_and.reduce(*self.using_val)
        else:
            return logic.reduce(*self.using_val)

    def after_select(self, eclick, erelease):
        """ 
        should be delet and keep only onselect.
        """
        pass


class Gfilter(Select):
    """ filtre graphiquement les objet Dat, voir Dat.filter """
    def __init__(self, ax, x, y, line, line_err, obj, reverse = False, **kargs):
        self.obj = obj
        self.line = line
        self.err = line_err
        super().__init__(ax, line, reverse)
        self.x = x
        self.y = y

    def onselect(self, eclick, erelease):
        p = self.select(eclick, erelease)
        if eclick.button == 1:
            op = self.obj.accept_val
            op[p] = False
            self.obj.accept_val = op
        else :
            op = self.obj.accept_val
            op[p] = True
            self.obj.accept_val = op
        self.line.set_data(self.x[self.obj.accept_val], self.y[self.obj.accept_val])
        self.err.set_data(self.x[~self.obj.accept_val], self.y[~self.obj.accept_val])
        plt.draw()


class Gfit(IncrSelect):
    def __init__(self, ax, line, model, pars, func=None, params=(), loc=1):
        """ func fonction à passé qui sera exécuté avec le paramètre out et params """
        self.model = model
        self.func = func
        self.pars = pars
        self.params_func = params # autre paramètre à donner à la fonction 
        self.out = None
        if self.func is None :
            self.fitcurve, = ax.plot([], [], '-', scaley=False)

        self.text = offsetbox.AnchoredText('', loc=loc)
        ax.add_artist(self.text)   

        IncrSelect.__init__(self, ax, line)
        self.x_eval = np.linspace(self.x[0].min(), self.x[0].max(), 2000)
        for erri in self.err:
            erri.set_alpha(0.9)

    def after_select(self, eclick, erelease):
        p = np.logical_not(self.using_val.squeeze())
        out = self.model.fit(self.y[0][p], self.pars, x=self.x[0][p] )
        if not self.func is None:
            self.func(out, *self.params_func)
        else :
            self.fitcurve.set_data(self.x_eval, self.model.eval(out.params, x=self.x_eval))
        self.out = out
        self.text.txt.set_text(self.format_res(out))

    def process(self):
        self.redraw()
        self.after_select(None,None)

    def format_res(self, out):
        s = out.fit_report()
        print(s)
        p = s.index(r'[[Variables]]')
        s = re.sub(r'\(init=.*\)', '', 'Variables :'+s[p+13:])
        s = re.sub(r'\[\[Correlations\]\] \(unreported correlations are <  0.100\)', 'Correlations :', s)
        return s

