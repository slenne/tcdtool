import matplotlib.pyplot as plt
from matplotlib.collections import LineCollection
from matplotlib import colors as mcolors

import numpy as np

class ColorLine(LineCollection):
    def __init__(self, x, y, c, cm='viridis', ax=None, n=100, cmin=None, cmax=None, **kargs):
        self._size = n
        if isinstance(cm, str):
            self._cm = plt.cm.get_cmap(cm)
        else:
            self._cm = cm
        self._cmin = cmin
        self._cmax = cmax
        segs = self.detSegments(x,y)
        colors = self.detColors(c)
        super().__init__(segs, colors=colors, **kargs)
        if ax:
            ax.add_collection(self)
    def set_data(self, x, y, c):
        segs = self.detSegments(x,y)
        colors = self.detColors(c)
        self.set_segments(segs)
        self.set_color(colors)
    def detSegments(self, x, y):
        st = len(x)/self._size
        segs = [ np.array([ x[int(i*st): int((i+1)*st)+1], y[int(i*st): int((i+1)*st+1)]]).T for i in range(self._size)]
        return segs
    def detColors(self, c):
        st = len(c)/self._size
        c = np.array(c)
        if self._cmin is None :
            if len(c) >0:
                cmin = np.nanmin(c)
            else:
                cmin = 0
        else :
            cmin = self._cmin
        if self._cmax is None:
            if len(c) >0:
                cmax = np.nanmax(c)
            else:
                cmax = 1
        else :
            cmax = self._cmax
        c -= cmin
        c /= (cmax - cmin)
        segs = [c[int(i*st): int((i+1)*st)+1].mean() for i in range(self._size)]
        return self._cm(segs)




