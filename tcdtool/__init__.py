
from .fit_fct import Select, Gfit, IncrSelect
from .datfile import ReadAuto as  ReadAuto_old, quick_show, LivePlot, splitDatfile, DatFile, Dat

readAuto = DatFile.readAuto


def ReadAuto(*args, **kargs):
    print('Deprecated: use readAuto instead')
    return ReadAuto_old(*args, **kargs)
