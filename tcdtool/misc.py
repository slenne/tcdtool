import numpy as np
import matplotlib.pyplot as plt
import tcdtool as tl
from scipy.optimize import newton
import lmfit
from scipy.signal import savgol_filter as savgol
import matplotlib.offsetbox as offsetbox
from .FL import FL

def vdp_eq(rs, rv, rh):
    return np.exp(-np.pi*rv/rs) + np.exp(-np.pi*rh/rs) -1.0

def get_rs(rv, rh):
    y = np.pi*(rv+rh)/(2*np.log(2))
    rs = newton(vdp_eq, y, args=(rv,rh))
    return rs

class EHE:
    def __init__(self, ax):
        self.ax = ax
        self.fitcurve1, = ax.plot([], [], '-')    
        self.fitcurve2, = ax.plot([], [], '-')    
        self.is_one = True 
        self.text = offsetbox.AnchoredText('', loc=2)
        ax.add_artist(self.text)   
        self.intercept1 = np.nan
        self.intercept2 = np.nan
        self.slope1 = np.nan
        self.slope2 = np.nan
        ax.figure.canvas.mpl_connect('key_press_event', self.on_press)
    def __call__(self, out):
        x = np.array(self.ax.get_xlim())
        a, b = out.values['slope'], out.values['intercept']
        if self.is_one :
            self.intercept1 = out.values['intercept']
            self.slope1 = out.values['slope']
            self.fitcurve1.set_data(x, a*x+b)
        else :
            self.intercept2 = out.values['intercept']
            self.slope2 = out.values['slope']
            self.fitcurve2.set_data(x, a*x+b)
        self.text.txt.set_text('diff = {:.3f}'.format(np.abs(self.intercept2-self.intercept1)*1000/2))
    def on_press(self, event):
        if event.key == ' ' :
            self.is_one = not self.is_one

def fct(x, a=1, b=0, c=0):
    z = np.zeros(x.size)
    p = x <0
    z[p] = a*x[p]+b
    z[~p] = a*x[~p]+c
    return z

m_fit_ampl_shift = lmfit.Model(fct)

class GetAmplShiftHc(tl.IncrSelect):
    def __init__(self, ax, line, param=None, **kargs):
        if param is None:
            self.param = {}
        else :
            self.param = param
        self.model = m_fit_ampl_shift
        self.out = None
        self.fitcurve, = ax.plot([], [], '-', scaley=False)
        #self.remanence, = ax.plot([], [], 'og', scaley=False)
        self.lhc_neg = ax.axvline(x=np.nan, linestyle='--', color='r')
        self.lhc_pos = ax.axvline(x=np.nan, linestyle='--', color='r')
        super().__init__(ax, line, **kargs)
        ax.figure.canvas.mpl_connect('key_press_event', self.onPress)
        self.x_eval = np.linspace(self.x[0].min(), self.x[0].max(), 2000)
        p = self.y[0].argmax()
        if self.x[0][p] < 0:
            loc =1
        else :
            loc = 2
        self.text = offsetbox.AnchoredText('', loc=loc)
        ax.add_artist(self.text)   

    def symetrise(self):
        """ Symetrise the limit for the field """
        vmin = np.min(self.x[0][~self.using_val.squeeze()])
        vmax = np.max(self.x[0][~self.using_val.squeeze()])
        vnew = (vmax-vmin)/2
        self.using_val[0,:] = np.logical_or(self.x[0] > vnew, self.x[0] < -vnew)

    def onPress(self, event):
        """ connect space to symetrise """
        if event.key == ' ':
            self.symetrise()
            self.process()
            self.redraw()

    def after_select(self, eclick, erelease):
        self.process()

    def process(self):
        out = self.model.fit(self.y[0][self.using_val.squeeze()], None, x=self.x[0][self.using_val.squeeze()] )
        self.fitcurve.set_data(self.x_eval, self.model.eval(out.params, x=self.x_eval))
        self.out = out
        a, b, c = self.out.values['a'], self.out.values['b'], self.out.values['c']
        ampl = c-b
        shift = (c+b)/2
        hc_neg_slope, hc_pos_slope = self.get_hc(self.x[0], self.y[0]-shift-a*self.x[0])
        # use the previouse value to determine the Hc without slope correction
        # we cut the loop before the signal cross 0 (using the average of previous value and the
        # solution given by the slope and the amplitude
        xlim_neg = (-abs(ampl/(2*a))+hc_neg_slope)/2
        xlim_pos = (abs(ampl/(2*a))+hc_pos_slope)/2
        p = np.logical_and(self.x[0]>xlim_neg, self.x[0]<xlim_pos)
        try:
            hc_neg, hc_pos = self.get_hc(self.x[0][p], self.y[0][p]-shift)
        except IndexError:
            hc_neg, hc_pos = np.nan, np.nan
        ampl_at0 = self.getRemanence(self.x[0], self.y[0] - shift)

        self.param.update({  'slope' : a,
                          'ampl' : ampl,
                          'shift' : shift,
                          'hc_neg' : hc_neg,
                          'hc_pos' : hc_pos,
                          'hc_neg_slope_correction' : hc_neg_slope,
                          'hc_pos_slope_correction' : hc_pos_slope,
                          } )
        #self.remanence.set_data([0,0], [shift-ampl_at0/2, shift+ampl_at0/2])
        self.lhc_neg.set_xdata([hc_neg_slope, hc_neg_slope])
        self.lhc_pos.set_xdata([hc_pos_slope, hc_pos_slope])
        legend = "slope : {:.4e}\nampl : {:.4e}\nremanence : {:.4e}\nshift : {:.4e}\nhc_neg : {:.4e}\nhc_pos : {:.4e}".format(a, ampl, ampl_at0, shift, hc_neg, hc_pos)
        self.text.txt.set_text(legend)

    @staticmethod
    def getRemanence(x, y):
        p0 = np.abs(x) < 0.01 # consider that we have alway some point between -0.01 and 0.01 T need to be improved
        p1 = np.logical_and(y > 0, p0)
        p2 = np.logical_and(y < 0, p0)
        amplat0 = y[p1].mean()-y[p2].mean()
        return amplat0

    @staticmethod
    def get_hc(x, y):
        p = x<0
        pl = np.argmin(np.abs(y[p]))
        if y[p][pl] < 0:
            if y[p][pl+1] >0 :
                pl1 = pl+1
            else :
                pl1 = pl-1
        else :
            if y[p][pl+1] <0 :
                pl1 = pl+1
            else :
                pl1 = pl-1
        hc_neg = x[p][pl]-y[p][pl]*(x[p][pl1]-x[p][pl])/(y[p][pl1]-y[p][pl])
        pl = np.argmin(np.abs(y[~p]))
        if y[~p][pl] < 0:
            if y[~p][pl+1] >0 :
                pl1 = pl+1
            else :
                pl1 = pl-1
        else :
            if y[~p][pl+1] <0 :
                pl1 = pl+1
            else :
                pl1 = pl-1
        hc_pos = x[~p][pl]-y[~p][pl]*(x[~p][pl1]-x[~p][pl])/(y[~p][pl1]-y[~p][pl])
        return hc_neg, hc_pos

    @staticmethod
    def plotFLfit(ax, line, param, include_shift=False ):
        """ plot the information line use in the GetAmplShiftHc 
        method in order to save it later
        x is the x data to plot
        param is the param use in GetAmplShiftHc, include_shift
        allow to shift or not the plot 
        """
        x, y = line.get_data()
        p = y.argmax()
        if x[p] < 0:
            loc =1
        else :
            loc = 2
        if include_shift:
            shift = param['shift']
        else:
            shift = 0
        slope = param['slope']
        ampl = param['ampl']
        hc_neg = param['hc_neg']
        hc_pos = param['hc_pos']
        ampl_at0  = param['remanence']
        legend = "slope : {:.4e}\nampl : {:.4e}\nremanence : {:.4e}\nshift : {:.4e}\nhc_neg : {:.4e}\nhc_pos : {:.4e}".format(slope, ampl, ampl_at0, shift, hc_neg, hc_pos)
        ax.plot(x, fct(x, slope, shift-ampl/2, ampl/2+shift), '-')
        ax.plot([0,0], [shift-ampl_at0/2, shift+ampl_at0/2], 'og')
        ax.axvline(x=hc_neg, linestyle='--', color='r')
        ax.axvline(x=hc_pos, linestyle='--', color='r')
        text = offsetbox.AnchoredText(legend, loc=loc)
        ax.add_artist(text)   


class FigTool:
    def __init__(self, fig, fname):
        self.fname = fname
        self.fig = fig
        self.fig.canvas.mpl_connect('key_press_event', self.onpress)
    def onpress(self, event):
        if event.key == 'w':
            self.fig.savefig(self.fname)
            print('fichier sauver : {}'.format(self.fname))



def linear_slope(x, y):
    a1 = np.polyfit(x, y, 1)[0]
    a2 = np.polyfit(-y, x, 1)[0]
    if np.abs(a1) < np.abs(a2) :
        phi = np.arctan(a1)
    else :
        phi = np.arctan(a2)-np.pi/2
    return phi

def rotate(x, y, phi):
    x1, y1 = np.cos(phi)*x+np.sin(phi)*y, -np.sin(phi)*x + np.cos(phi)*y
    return x1, y1



def nsavgol(x, y, a, xev=None, order=1, diff=0):
    if xev is None:
        xev = x
    # ensure data are sorted
    p = np.argsort(x)
    x, y = x[p], y[p]
    p1 = np.searchsorted(x, xev-a)
    p2 = np.searchsorted(x, xev+a, side='right')
    k = [ np.polyfit(x[pi1:pi2], y[pi1:pi2], order) for pi1, pi2 in zip(p1,p2) ]
    if diff != 0:
        k = [np.polyder(x, diff) for x in k]
    val = [ np.polyval(ki, xi) for ki, xi in zip(k, xev) ] 
    return np.array(val)


def sigmaFilter(x, y, ma, n_sigma=3):
    """ run a fit eliminating point that are more than n_sigma*sigma """
    p = np.ones_like(x, dtype=bool)
    ref = -1
    nb_params = 0
    params = ma.make_params()
    nb_run = 0
    while np.count_nonzero(p) != ref:
        ref = np.count_nonzero(p) 
        out = ma.fit(y[p], x=x[p], params=params)
        p[p] = np.abs(out.residual) < n_sigma * np.std(out.residual)
        nb_run += 1
        params = out.params
    return nb_run, p, params


def flattenAngle(x):
    """ recreate continusly increasing angle for the mulitmag"""
    x = x.copy()
    x0 = x[0]
    for i,xi in enumerate(x):
        xi = x0 + (xi-x0+np.pi)%(np.pi*2) - np.pi
        x[i] = xi
        x0 = xi
    return x




def splitturn(tin, plot=False):
    """Split the the different field intensity for the multimag """
    tin.add(np.arange(len(tin.accept_val)), 'nb point', 'nb_point')
    dfi = tin.getVal('field')
    dfi = savgol(dfi, 5, 1, 1)
    if plot:
        fig, ax = plt.subplots()
        plt.plot(dfi)
    p = []
    run = False
    buff = []
    for i,x in enumerate(dfi):
        if x > 1.5e-2:
            run = True
            buff.append(i)
        elif run :
            run = False
            p.append(buff[len(buff)//2])
            buff = []
    tsp = tl.splitDatfile(tin, p)
    if plot:
        fig, ax = plt.subplots() 
        for t in tsp:
            t.plot(ax, 'field')
        plt.show()
    return tsp


